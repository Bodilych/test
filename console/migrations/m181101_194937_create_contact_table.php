<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contact`.
 */
class m181101_194937_create_contact_table extends Migration
{
    /**
     * @var string
     */
    protected $table = 'contact';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey()->unsigned(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
