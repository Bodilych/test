<?php

use yii\db\Migration;

/**
 * Handles the creation of table `phone`.
 */
class m181101_194938_create_phone_table extends Migration
{
    /**
     * @var string
     */
    protected $table = 'phone';

    /**
     * @var string
     */
    protected $columnContactId = 'contact_id';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            $this->columnContactId => $this->integer()->unsigned()->notNull(),
            'phone' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->db->driverName === 'mysql' ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB' : null);

        $this->createIndex("idx-$this->table-$this->columnContactId", $this->table, $this->columnContactId);
        $this->addForeignKey(
            "fk-$this->table-$this->columnContactId",
            $this->table, $this->columnContactId,
            'contact',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk-$this->table-$this->columnContactId", $this->table);
        $this->dropIndex("idx-$this->table-$this->columnContactId", $this->table);
        $this->dropTable($this->table);
    }
}
