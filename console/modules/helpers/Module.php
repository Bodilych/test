<?php
/**
 * Created by PhpStorm.
 * User: Bodilych
 * Date: 02.11.2018
 * Time: 0:02
 */

namespace console\modules\helpers;

/**
 * Class Module
 * @package console\modules\helpers
 */
class Module extends \yii\base\Module
{
    /**
     * @var int
     */
    public $newFileMode = 0666;

    /**
     * @var int
     */
    public $newDirMode = 0777;
}