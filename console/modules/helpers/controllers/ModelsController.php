<?php
/**
 * Created by PhpStorm.
 * User: Bodilych
 * Date: 01.11.2018
 * Time: 21:51
 */

namespace console\modules\helpers\controllers;

use yii\console\Controller;
use yii\db\Connection;
use yii\di\Instance;
use yii\gii\CodeFile;
use yii\gii\generators\model\Generator;
use yii\helpers\FileHelper;
use yii\helpers\Inflector;
use Yii;

/**
 * Class ModelsController
 * @package console\controllers
 */
class ModelsController extends Controller
{
    /**
     * @var string
     */
    public $defaultNs = 'common\models\core';

    /**
     * @var string
     */
    public $defaultDirectory = '@common/models/core';

    /**
     * @var array
     */
    public $skipTables = [
        'migration',
    ];

    /**
     * @var string
     */
    public $defaultAction = 'generate';

    /**
     * @var string|array|Connection
     */
    public $db = 'db';

    /**
     * @var string[]
     * @example [
     *      'table' => 'TableTest',
     *      ...
     * ]
     */
    public $classNames = [];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init(): void
    {
        parent::init();
        $this->db = Instance::ensure($this->db, Connection::class);
        $this->defaultDirectory = Yii::getAlias($this->defaultDirectory);
    }

    /**
     * Generate core models
     * @throws \yii\base\ErrorException
     * @throws \yii\base\Exception
     */
    public function actionGenerate(): void
    {
        $this->removeDirectory();
        $this->createDirectory();

        foreach ($this->getTables() as $table) {
            if (\in_array($table, $this->skipTables, true)) {
                continue;
            }
            $this->stdout("$table\n");
            $this->saveFiles($this->getGenerator($table, $this->getClassName($table))->generate());
        }
    }

    /**
     * @param CodeFile[] $files
     */
    protected function saveFiles(array $files): void
    {
        foreach ($files as $file) {
            $file->save();
        }
    }

    /**
     * @param string $table
     * @return string
     */
    protected function getClassName(string $table): string
    {
        return $this->classNames[$table] ?? Inflector::id2camel($table);
    }

    /**
     * @param string $tableName
     * @param string $modelClass
     * @return Generator
     */
    protected function getGenerator(string $tableName, string $modelClass): Generator
    {
        return new Generator([
            'ns' => $this->defaultNs,
            'tableName' => $tableName,
            'modelClass' => $modelClass,
        ]);
    }

    /**
     * @return string[]
     */
    protected function getTables(): array
    {
        return $this->db->schema->tableNames;
    }

    /**
     * @throws \yii\base\ErrorException
     */
    protected function removeDirectory(): void
    {
        FileHelper::removeDirectory($this->defaultDirectory);
    }

    /**
     * @return bool
     * @throws \yii\base\Exception
     */
    protected function createDirectory(): bool
    {
        return FileHelper::createDirectory($this->defaultDirectory);
    }

}