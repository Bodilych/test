**For run**

* Apache
* Php 7.2
* MySql 5.8
* Redis 3.2

1. composer install
2. php init
3. set credential to .env
4. php yii migrate
5. php yii helpers/models - for generation core models
6. php yii queue/listen

**For test**

POST request on http://[YOUR_HOST]/api/v1/contacts

View result on http://[YOUR_HOST]