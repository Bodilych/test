<?php

use yii\db\Connection as DB;
use yii\caching\FileCache;
use yii\redis\Connection as Redis;
use yii\queue\redis\Queue;
use yii\queue\LogBehavior;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'bootstrap' => [
        'queue',
    ],
    'vendorPath' => dirname(__DIR__, 2) . '/vendor',
    'components' => [
        'cache' => [
            'class' => FileCache::class,
        ],
        'db' => [
            'class' => DB::class,
            'dsn' => sprintf('mysql:host=%s;dbname=%s', getenv('DB_HOST'), getenv('DB_NAME')),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'charset' => 'utf8',
        ],
        'redis' => [
            'class' => Redis::class,
            'database' => 0,
        ],
        'redis_queue' => [
            'class' => Redis::class,
            'database' => 1,
            'retries' => 1,
        ],
        'queue' => [
            'class' => Queue::class,
            'redis' => 'redis_queue',
            'as log' => [
                'class' => LogBehavior::class,
            ]
        ],
    ],
];
