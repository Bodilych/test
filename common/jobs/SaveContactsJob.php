<?php
/**
 * Created by PhpStorm.
 * User: Bodilych
 * Date: 02.11.2018
 * Time: 12:47
 */

namespace common\jobs;

use yii\queue\JobInterface;
use yii\queue\Queue;
use common\models\redis\Contact as ContactRedis;
use common\models\Contact;
use common\models\Phone;
use Exception;

/**
 * Class SaveContactJob
 * @package common\jobs
 */
class SaveContactsJob implements JobInterface
{
    /**
     * @param Queue $queue
     * @throws Exception
     */
    public function execute($queue): void
    {
        foreach ($this->getContacts() as $contact) {
            $model = $this->saveContact($contact);
            if ($model === null) {
                throw new Exception('Contact is not saved');
            }
            foreach ($contact->phoneNumbers as $phoneNumber) {
                if (!$this->savePhone($model->id, $phoneNumber)) {
                    throw new Exception('Phone is not saved');
                }
            }
            $contact->delete();
        }
    }

    /**
     * @return ContactRedis[]
     */
    protected function getContacts(): array
    {
        return ContactRedis::find()->all();
    }

    /**
     * @param ContactRedis $contact
     * @return Contact|null
     */
    protected function saveContact(ContactRedis $contact): ?Contact
    {
        $model = new Contact();
        $model->first_name = $contact->firstName;
        $model->last_name = $contact->lastName;
        if ($model->save()) {
            return $model;
        }
        return null;
    }

    /**
     * @param int $contact_id
     * @param string $phone
     * @return bool
     */
    protected function savePhone(int $contact_id, string $phone): bool
    {
        $model = new Phone();
        $model->contact_id = $contact_id;
        $model->phone = $phone;
        return $model->save();
    }
}