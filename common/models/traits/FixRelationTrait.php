<?php
/**
 * Created by PhpStorm.
 * User: Bodilych
 * Date: 02.11.2018
 * Time: 0:57
 */

namespace common\models\traits;

/**
 * Trait FixRelationTrait
 * @package common\models\traits
 */
trait FixRelationTrait
{
    /**
     * @param $class
     * @param $link
     * @param $multiple
     * @return mixed
     */
    protected function createRelationQuery($class, $link, $multiple)
    {
        return parent::createRelationQuery(str_replace('\core', '', $class), $link, $multiple);
    }
}