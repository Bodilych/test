<?php
/**
 * Created by PhpStorm.
 * User: Bodilych
 * Date: 02.11.2018
 * Time: 0:23
 */

namespace common\models;

use common\models\core\Phone as Core;
use yii\behaviors\TimestampBehavior;
use common\models\traits\FixRelationTrait;

/**
 * Class Phone
 * @package common\models
 *
 * @property Contact $contact
 */
class Phone extends Core
{
    use FixRelationTrait;

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return array_merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::class,
            ]
        ]);
    }
}