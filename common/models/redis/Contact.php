<?php
/**
 * Created by PhpStorm.
 * User: Bodilych
 * Date: 06.11.2018
 * Time: 16:57
 */

namespace common\models\redis;

use yii\redis\ActiveRecord;

/**
 * Class Contact
 * @package common\models\redis
 *
 * @property string $firstName
 * @property string $lastName
 * @property array|string $phoneNumbers
 */
class Contact extends ActiveRecord
{
    /**
     * @return array
     */
    public function attributes(): array
    {
        return [
            'id',
            'firstName',
            'lastName',
            'phoneNumbers',
        ];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['firstName', 'lastName'], 'required'],
            ['phoneNumbers', 'safe'],
        ];
    }

    /**
     * @param array $data
     * @param null $formName
     * @return bool
     */
    public function load($data, $formName = null): bool
    {
        if (parent::load($data, $formName) && \is_array($this->phoneNumbers)) {
            $this->phoneNumbers = serialize($this->phoneNumbers);
            return true;
        }
        return false;
    }

    public function afterFind()
    {
        if (\is_string($this->phoneNumbers)) {
            $this->phoneNumbers = unserialize($this->phoneNumbers, ['allowed_classes' => true]);
        }
    }
}