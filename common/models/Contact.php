<?php
/**
 * Created by PhpStorm.
 * User: Bodilych
 * Date: 02.11.2018
 * Time: 0:58
 */

namespace common\models;

use common\models\core\Contact as Core;
use common\models\traits\FixRelationTrait;
use yii\behaviors\TimestampBehavior;

/**
 * Class Contact
 * @package common\models
 *
 * @property Phone[] $phones
 */
class Contact extends Core
{
    use FixRelationTrait;

    /**
     * @return array
     */
    public function behaviors():array
    {
        return array_merge(parent::behaviors(),[
            TimestampBehavior::class,
        ]);
    }
}