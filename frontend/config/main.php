<?php

use yii\base\Module;
use yii\rest\ActiveController;
use common\models\redis\Contact;
use yii\rest\UrlRule as RestUrlRule;
use yii\base\Application;
use yii\base\Event;
use common\jobs\SaveContactsJob;

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
$baseUrl = '';
return [
    'id' => 'app-frontend',
    'defaultRoute' => 'contacts',
    'homeUrl' => $baseUrl,
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'on ' . Application::EVENT_BEFORE_REQUEST => function (Event $event) {
        /** @var Application $app */
        $app = $event->sender;
        Event::on(Contact::class, Contact::EVENT_AFTER_INSERT, function () use ($app) {
            $app->queue->push(new SaveContactsJob());
        });
    },
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-frontend',
            'baseUrl' => $baseUrl,
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                [
                    'class' => RestUrlRule::class,
                    'controller' => ['api/v1/contacts'],
                    'only' => ['create'],
                ],
                'site' => 'site',
                'site/index' => 'site',
                'site/about' => 'site/about',
                'site/contact' => 'site/contact',
                'site/signup' => 'site/signup',
                'site/login' => 'site/login',
                'contacts' => 'contacts',
                'contacts/index' => 'contacts',
                '/' => '/',
            ],
        ],
    ],
    'modules' => [
        'api' => [
            'class' => Module::class,
            'modules' => [
                'v1' => [
                    'class' => Module::class,
                    'controllerMap' => [
                        'contacts' => [
                            'class' => ActiveController::class,
                            'modelClass' => Contact::class,
                        ],
                    ],
                ],
            ],
        ],
    ],
    'params' => $params,
];
